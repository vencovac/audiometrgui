function playPsychPortAudio(input,fsamp,channel);
% communication with the sound card by using psychportaudio (psychtoolbox)


    clear PsychPortAudio; % clear portaudio (
    PsychPortAudio('Verbosity', 5); %
    %oldlevel = PsychPortAudio(�Verbosity� [,level]);
    %Set level of verbosity for error/warning/status messages. �level� optional, new
    %level of verbosity. �oldlevel� is the old level of verbosity. The following
    %levels are supported: 0 = Shut up. 1 = Print errors, 2 = Print also warnings, 3
    %= Print also some info, 4 = Print more useful info (default), >5 = Be very
    %verbose (mostly for debugging the driver itself).
    PlayChanList = channel; % play through single channel
    MasterPlayList = max(channel);
    mode = 1; % play mode (play and record)
    reqlatencyclass = 1; % latency class (0 - 3)
    buffersize = 2048; % buffersize
    
    dev = PsychPortAudio('GetDevices'); % get list of all devices
    for k=1:length(dev), if contains(dev(k).DeviceName,'ASIO Fireface'), devID = dev(k).DeviceIndex; end, end;
    pamaster = PsychPortAudio('Open', devID, mode+8, reqlatencyclass, fsamp,MasterPlayList,buffersize,0.005);
    %pahandle = PsychPortAudio(�Open� [, deviceid][, mode][, reqlatencyclass][, freq][, channels][, buffersize][, suggestedLatency][, selectchannels][, specialFlags=0]);
    PsychPortAudio('Start', pamaster,0,0,1);
    
    pahandle = PsychPortAudio('OpenSlave', pamaster, mode, length(PlayChanList),PlayChanList);
    %pahandle = PsychPortAudio(�Open� [, deviceid][, mode][, reqlatencyclass][, freq][, channels][, buffersize][, suggestedLatency][, selectchannels][, specialFlags=0]);
    
    InputSig = [input.'];
    
    PsychPortAudio('FillBuffer', pahandle, InputSig);
    %[underflow, nextSampleStartIndex, nextSampleETASecs] = PsychPortAudio(�FillBuffer�, pahandle, bufferdata [, streamingrefill=0][, startIndex=Append]);
    TimeDur = size(InputSig,2)*1/fsamp + 0.2;
   
    %[audiodata absrecposition overflow cstarttime] = PsychPortAudio(�GetAudioData�, pahandle [, amountToAllocateSecs][, minimumAmountToReturnSecs][, maximumAmountToReturnSecs][, singleType=0]);
    PsychPortAudio('Start', pahandle,1,0,0);
    pause(TimeDur); % pause to send data and receive signal
        
    Status= PsychPortAudio('GetStatus', pahandle);
    [startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pahandle, 1);
    %[startTime endPositionSecs xruns estStopTime] = PsychPortAudio(�Stop�, pahandle [, waitForEndOfPlayback=0] [, blockUntilStopped=1] [, repetitions] [, stopTime]);
    PsychPortAudio('Close');% Close the audio device:
    clear PsychPortAudio; % clear portaudio
    
    if Status.XRuns>0
        fprintf('Status: xrun detected\n');
    end
    
    if xruns>0
        fprintf('xrun detected\n');
    end