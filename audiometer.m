 function audiometer(in)
% function audiometer(in)
%
% AUDIOMETER
% aplication for measurement of pure tone audiogram (it is supposed
% to use Modified Hugson-Westlake method)
% 
% Vaclav Vencovsky, 2011, vencovac@fel.cvut.cz

if nargin==0

    clc; close all; %clear all;

    % gui - initialization
    
    fig = figure('position',[150 250 950 400],'menubar','none','name','audiometer','numbertitle','off','ButtonDownFcn', @mouseclick_callback);
    % now attach the function to the axes
    % set(gca,'ButtonDownFcn', @mouseclick_callback)

    graph = axes('position',[0.3 0.12 0.53 0.8],'tag','graph','nextplot','add');
    set(graph,'xscale','log');
    set(graph,'xlim',[125 16000]);
    set(graph,'ylim',[-120 20]);
    %get(graph,'ytick')
    set(graph,'yticklabels',-1*get(graph,'ytick'));
    grid on;
    box on;
    set(graph,'xtick',[125 250 500 1000 2000 4000 8000 16000]);
    plot(graph,[125 16e3],[0 0],'linewidth',1.5,'color','k');
    plot(graph,[125 16e3],[-20 -20],'linewidth',0.8,'color','k','linestyle','-');
    xlabel('Frequency (Hz)');
    ylabel('Level (dB re abs. threshold)');
    
    
    leveCalib = uicontrol(fig,'units','normalized','style','edit','position',[0.02 0.95 0.12 0.05],'string','HDA-300left01.mat','tag','AMcalL');
    praveCalib = uicontrol(fig,'units','normalized','style','edit','position',[0.02 0.9 0.12 0.05],'string','HDA-300right01.mat','tag','AMcalP');
    
    RETSPLlevel = uicontrol(fig,'units','normalized','style','edit','position',[0.14 0.95 0.12 0.05],'string','RETSPLhda300.mat','tag','AMretsplL');
    RETSPLprave = uicontrol(fig,'units','normalized','style','edit','position',[0.14 0.9 0.12 0.05],'string','RETSPLhda300.mat','tag','AMretsplP');
    
    ear = uicontrol(fig,'units','normalized','style','popupmenu','position',[0.05 0.80 0.13 0.08],'string','left ear|right ear','tag','AMear');

    freq_text = uicontrol(fig,'units','normalized','style','text','position',[0.05 0.7 0.13 0.1],'string','Frequency (Hz)');
    
    freq_value = uicontrol(fig,'units','normalized','style','edit','position',[0.05 0.65 0.13 0.1],'string','1000','tag','freq_value');

    level_text = uicontrol(fig,'units','normalized','style','text','position',[0.05 0.50 0.13 0.1],'string','Level (dB re abs. threshold)');

    level_value = uicontrol(fig,'units','normalized','style','edit','position',[0.05 0.40 0.13 0.1],'string','50','tag','level_value');
    level_v_down = uicontrol(fig,'units','normalized','style','push','position',[0.05 0.35 0.065 0.05],'string','down','tag','level_value_down','callback','audiometer down');
    level_v_up = uicontrol(fig,'units','normalized','style','push','position',[0.115 0.35 0.065 0.05],'string','up','tag','level_value_up','callback','audiometer up');

    play = uicontrol(fig,'style','push','units','normalized','position',[0.05 0.2 0.13 0.13],'string','play','tag','play','callback','audiometer play');

    save_r = uicontrol(fig,'style','push','units','normalized','position',[0.05 0.05 0.13 0.13],'string','save','tag','save','callback','audiometer save_r');

    % buttons on the other right side  (save results and add calibration curve)
    save_all = uicontrol(fig,'style','push','units','normalized','position',[0.85 0.05 0.13 0.13],'string','save results','tag','save_all','callback','audiometer save_all');

    indicator= uicontrol(fig,'units','normalized','style','text','position',[0.85 0.50 0.13 0.1]', 'BackGroundColor', [.8 .8 .8], 'Tag', 'indicator');

    print = uicontrol(fig,'style','push','units','normalized','position',[0.85 0.7 0.13 0.13],'string','Print Results','tag','save_all','callback','audiometer print_all');


    % and we also have to attach the function to the children, in this
    % case that is the line in the axes.

    set(get(gcf,'Children'),'ButtonDownFcn', @mouseclick_callback)
    hold on;

else
    
    switch(in)
        
        case 'play'
            addpath('fce_psychportaudio/')
            % signal generation (tone)
            fs = 44.1e3; % sample freq
            t = 2; % duration of the signal (seconds)
            freq = str2num(get(findobj('tag','freq_value'),'string')); % get freq of tone
            level = str2num(get(findobj('tag','level_value'),'string')); % get level of tone
            
            if level>70
                level = 70;
                set(findobj('tag','level_value'),'string', '70');
                
            end;
            ear = get(findobj('tag','AMear'),'value'); % which ear is going to be tested
            
            tx = (0:1/fs:t-1/fs)';
            global tone; 
            tone = sin(2*pi*freq*tx); 
            
            % ramping
            ramp_dur = 50e-3; % ms duration of ramp
            x = [0:1/fs:ramp_dur]';
            x = pi*x/ramp_dur;
            rampUp = 0.5*(1 - cos(x));
            rampDown = flipud(rampUp);
            whole_ramp = [rampUp; ones(length(tone)-2*length(x),1); rampDown(1:end)];

            tone = tone.*whole_ramp; % shape by ramp
            
            % set level of the tone (call calibration sript) to get offset
                                  
                        
%            scale1 = giveNeededAmplitude(level,freq,fs);
            
            %load LevePokus_310120.mat;            
            
            %HresS = smooth(abs(TrFuncAE),30,'loess');
%fs = 96e3;

%fxA = (0:length(RecEarInAbs)-1)*fs/length(RecEarInAbs);
    
            %if 
            ear = get(findobj('tag','AMear'),'value'); % get left or right ear
            if ear==1          
                load(get(findobj('tag','AMcalL'),'string'))
                load(get(findobj('tag','AMretsplL'),'string'))
                fxI = 1:1:20e3;
                AThreshI = interp1(ATfreq,AThresh,fxI,'spline'); % interpolation of RETSPL
                
            elseif ear==2
                load(get(findobj('tag','AMcalP'),'string'))
                load(get(findobj('tag','AMretsplP'),'string'))
                fxI = 1:1:20e3;
                AThreshI = interp1(ATfreq,AThresh,fxI,'spline'); % interpolation of RETSPL
                
            end
            
            HresS = abs(data.TrFuncAE);
            fxA=data.fx;
           
            fx = 100:1:max(15e3); % interpolated fx
            [Lx1H] = interp1(fxA,HresS,fx,'pchip');

%idxF = find(fx==freq);


             idxF = find(fx==freq);
             idxRETSPL = find(fxI==freq);
             levelSPL = level + AThreshI(idxRETSPL);   
             scale1 = 10.^(levelSPL/20)*sqrt(2)*2e-5./Lx1H(idxF);
           
            
             tone = tone*scale1;  % set desired level of tone            
        
            % soundcard settings
%              H = dsp.AudioPlayer
% 
%             devID = 'default';                    %device ID The default is Default, which 
%                                       %is the computer's standard output device. 
%                                       %You can use tab completion to query valid 
%                                       %DeviceName assignments for your computer by
%                                       %typing H.DeviceName = ' and then pressing the tab key.
%             bitDepth = '24-bit integer';  %output bitdepth
%             %outputCh = [1,2];             %vector containing output channels' numbers
%             bufferSize = 2048;
%            
%            
%             PlayChanList = [3];
% 
%             [unrun] = DSPplay(tone,devID,PlayChanList,bufferSize,fs);

            
            if ear==1
                channel = 7; % left ear
            elseif ear==2
                channel = 8; % right ear
            end
            playPsychPortAudio(tone,fs,channel);
            
            
%         m_outsig = tone;
%         playDeviceID = 0;
%         Fsplay = fs;
%         chanList = 5; % 5 for FEL
%         if(~playrec('isInitialised'))
%             fprintf('Initialising playrec to use sample rate: %d, playDeviceID: %d and no record device\n', Fsplay, playDeviceID);
%             playrec('init', Fsplay, playDeviceID, -1);
% 
%             % This slight delay is included because if a dialog box pops up during
%             % initialisation (eg MOTU telling you there are no MOTU devices
%             % attached) then without the delay Ctrl+C to stop playback sometimes
%             % doesn't work.
%             pause(0.1);
%         end
% 
%         if(~playrec('isInitialised'))
%             error ('Unable to initialise playrec correctly');
%         elseif(playrec('getPlayMaxChannel')<max(chanList))
%             error ('Selected device does not support %d output channels\n', max(chanList));
%         end
% 
%         if(playrec('pause'))
%             fprintf('Playrec was paused - clearing all previous pages and unpausing.\n');
%             playrec('delPage');
%             playrec('pause', 0);
%         end
%         playrec('play', m_outsig, chanList);
%                                
        rmpath('fce_psychportaudio/')    
        case 'down'
            level_value = findobj('tag','level_value');
            level = str2num(get(level_value,'string'));
            
            st = 5; % step of level change is 2.5 dB
            level = level - st;
            
            set(level_value,'string',num2str(level));
            
        case 'up'
            level_value = findobj('tag','level_value');
            level = str2num(get(level_value,'string'));
            
            st = 5; % step of level change is 2.5 dB
            level = level + st;
            
            set(level_value,'string',num2str(level));
            
            
        case 'save_r' % put data to graph
            
            level = str2num(get(findobj('tag','level_value'),'string'));
            freq = str2num(get(findobj('tag','freq_value'),'string'));
            
% % % % % % %             gr = findobj('tag','graph');
            ear = get(findobj('tag','AMear'),'value');
            if ear==1
                znak = '+';
            else
                znak = 'o';
            end
             
            plot(freq,-1*level,znak,'linewidth',1.2,'color','k');
            
        case 'save_all' % save data from graph to file 
            
            hDir = findobj('Tag','Esetdir'); % find handle to the folder name
            
             
            DT = datetime('now'); % get current time and date
             Folder = get(hDir,'string'); % get directory where to save recorded signals
             SubjName = get(findobj('Tag','ESubjName'),'string'); % get name of the subject
             Year = DT.Year-2000;
             YearS = num2str(Year);
             Month = DT.Month;
        
        
        
             if Month<10, MonthS = ['0' num2str(Month)]; else, MonthS = [num2str(Month)];end
             Day = DT.Day;
             if Day<10,DayS = ['0' num2str(Day)]; else, DayS = [num2str(Day)]; end
             Hour = DT.Hour;
             if Hour<10,  HourS = ['0' num2str(Hour)];else, HourS = [num2str(Hour)]; end
             Minute = DT.Minute; 
             if Minute<10, MinuteS = ['0' num2str(Minute)];else, MinuteS = [num2str(Minute)];end
             Second = round(DT.Second);
             if Second<10,SecondS = ['0' num2str(Second)];else, SecondS = [num2str(Second)];end

            
            freq= get(get(gca,'Children'),'xdata');
            level = get(get(gca,'Children'),'ydata');
            marker = get(get(gca,'Children'),'marker');
            if iscell(freq); freq = cell2mat(freq); end;
            if iscell(level); level = cell2mat(level); end;
            if iscell(marker); marker = cell2mat(marker); end;
            l_freq = [];
            r_freq = [];
            l_level = [];
            r_level = [];
            for k=1:size(freq,1)
                
                switch marker(k)
                    case '+'
                        l_freq = [l_freq freq(k)];
                        l_level = [l_level level(k)];
                    case 'o'
                        r_freq = [r_freq freq(k)];
                        r_level = [r_level level(k)];
                end
                       
             
            end
    
            
            % save results
          %  if ~isempty(l_freq)
                l_freq = l_freq';
                l_level = l_level';
                
               save([Folder '\AG_' SubjName '_' YearS MonthS DayS '_' HourS MinuteS SecondS ...
            '.mat'],...
            'l_freq','l_level');
        
        
                
                %[filename, pathname] = uiputfile('*.mat','save left ear data');
               % save([pathname filename],'l_freq','l_level');
                
          %  end
%             if ~isempty(r_freq)
%                 r_freq = r_freq';
%                 r_level = r_level';
%                 [filename, pathname] = uiputfile('*.mat','save right ear data');
%                 save([pathname filename],'r_freq','r_level');
%             end
%             
%         case 'print_all' % save data from graph to file     
%             freq= get(get(gca,'Children'),'xdata');
%             level = get(get(gca,'Children'),'ydata');
%             marker = get(get(gca,'Children'),'marker');
%             if iscell(freq); freq = cell2mat(freq); end;
%             if iscell(level); level = cell2mat(level); end;
%             if iscell(marker); marker = cell2mat(marker); end;
%             l_freq = [];
%             r_freq = [];
%             l_level = [];
%             r_level = [];
%             for k=1:size(freq,1)
%                 
%                 switch marker(k)
%                     case '+'
%                         l_freq = [l_freq freq(k)];
%                         l_level = [l_level level(k)];
%                     case 'o'
%                         r_freq = [r_freq freq(k)];
%                         r_level = [r_level level(k)];
%                 end
%                        
%              
%             end
            
%             figure;
%             x = axes('position',[0.1 0.1 0.8 0.8],'tag','x');
%             plot(l_freq,l_level, 'k+','MarkerSize', 10, 'LineWidth', 2); hold on; plot(r_freq,r_level, 'ko','MarkerSize', 10, 'LineWidth', 2);
%            set(x,'xscale','log');
%            set(x,'xlim',[125 16000]);
%             set(x,'ylim',[-20 60]);
%             grid on;
%             box on;
%            legend('absolute threshold left ear','absolute threshold right ear')
%            set(x,'xtick',[125 250 500 1000 2000 4000 8000 16000]);
%             xlabel('frequency (Hz)');
%             ylabel('level (dB re abs. threshold)');
    end
    
    
    
    

end
end
